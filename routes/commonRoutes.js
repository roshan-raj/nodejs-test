var express = require('express');
var app = express.Router();

const { sendResponse, formatAndReturnResponse } = require('com.thbs.responsehelper');

//==============================================================================

app.get('/v1', (req, res) => {
    sendResponse(res, formatAndReturnResponse(200, null, "Welcome to the NodeJs Application Template!"));
});

//==============================================================================

module.exports = app;