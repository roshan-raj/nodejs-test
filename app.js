var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require('cors')
const bodyParser = require('body-parser');

//Create express object
var app = express();

//============================================================================
//CORS
app.use(cors());

//==============================================================================

//Require `config` and `db` files
require('./environment_config');
//We use this file just to get the basic public details of the app. All the secure things are stored in .env file
// config variables
const config = require('./config/config.js');

//===============================================================================

app.use(bodyParser.urlencoded({ extended: true }));
// parse requests of content-type - application/json
app.use(bodyParser.json())
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.disable('x-powered-by');

//===============================================================================

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

//===============================================================================
//Require all the routes
const commonRoutes = require('./routes/commonRoutes');

app.use('/', commonRoutes);

//===============================================================================

module.exports = app;