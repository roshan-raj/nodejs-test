#!/bin/bash

# any future command that fails will exit the script
set -e

# Delete the old repo
rm -rf /home/ubuntu/nodejs-test/

# clone the repo again
git clone https://gitlab.com/roshan-raj/nodejs-test.git

# stop the previous pm2
pm2 stop nodeJSTest
pm2 delete nodeJSTest

cd /home/ubuntu/nodejs-test

#install npm packages
echo "Running npm install"
npm install --save

#Restart the node server
npm run start:testing
